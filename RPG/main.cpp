/**
 * @file main.cpp
 * @brief Entry Point
 * @date Dec 23, 2017
 * @author boo
 */

#include "MainWindow.h"

/// @struct WindowParam
struct WindowParam {
  //! window width
  int width;

  //! window heigth
  int height;

  //! window x position
  int x;

  //! window y position
  int y;
};


/// @brief function that read configuration files
/// @param path - configuration file path
/// @param wp - window param reference
/// @return if configuration file is read successfully, return true. Otherwise, return false
bool readConfig( const char *path, WindowParam &wp ) {
  FILE *fPtr;

  if( NULL == ( fPtr = fopen( path, "r" ) ) ) {
    printf( "%s file not opened\n", path );
    return false;
  }

  fclose( fPtr );

  return true;
}

/// @brief main function
int main( int argc, char **argv ) {
  WindowParam main_win_param;

  main_win_param.width = 545;
  main_win_param.height = 642;
  main_win_param.x = 600;
  main_win_param.y = 100;

  if( readConfig( "conf/map_window.txt", main_win_param ) ) {
    MainWindow main_window( main_win_param.width, main_win_param.height, "Boo RPG Game" );
    Vector2< int > main_win_pos = { main_win_param.x, main_win_param.y };
  
    main_window.setPosition( main_win_pos );

    if( main_window.init( "conf/obj_list.txt" ) ) {
      main_window.run();
    }
  }
  
  return 0;
}
