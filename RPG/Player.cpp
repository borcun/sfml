#include "Player.h"

Player::Player( const int objSign ) : GameObject( objSign ) {

}

Player::~Player() {

}

void Player::setGoldCount( const int pGoldCount ) {
  if( pGoldCount < 0 ) {
    printf( "Player gold count can not be negative\n" );
    return;
  }

  goldCount = pGoldCount;
  return;
}

void Player::setLevel( const int pLevel ) {
  if( pLevel < 0 ) {
    printf( "Player level can not be negative\n" );
    return;
  }

  level = pLevel;
  return;
}

void Player::setExperience( const int pExperience ) {
  if( pExperience < 0 ) {
    printf( "Player experience can not be negative\n" );
    return;
  }

  experience = pExperience;
  return;
}

void Player::setToLevelUp( const int pToLevelUp ) {
  if( pToLevelUp < 0 ) {
    printf( "Player to level up can not be negative\n" );
    return;
  }

  toLevelUp = pToLevelUp;
  return;
}

void Player::setPotionCount( const int pPotionCount ) {
  if( pPotionCount < 0 ) {
    printf( "Player potion count can not be negative\n" );
    return;
  }

  potionCount = pPotionCount;
  return;
}

int Player::getGoldCount( void ) const {
  return goldCount;
}

int Player::getLevel( void ) const {
  return level;
}

int Player::getExperience( void ) const {
  return experience;
}

int Player::getToLevelUp( void ) const {
  return toLevelUp;
}

int Player::getPotionCount( void ) const {
  return potionCount;
}

double Player::attack( void ) {
  double att = 1.0;

  printf( "Player attacked\n" );

  return att;
}

void Player::information( void ) {
  printf( "%s Object\n", getName().c_str() );
  return;
}

void Player::drinkPotion( void ) {
  printf( "Player drunk potion\n" );
  return;
}

void Player::addExperience( const int pExperience ) {
  printf( "Experience [%d] is added to player\n", pExperience );
  return;
}

void Player::addGold( const int goldAmount ) {
  goldCount += goldAmount;
  return;
}

void Player::levelUp( void ) {
  printf( "Player level up\n" );
  return;
}
  

