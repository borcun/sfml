#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <iostream>
#include <cstdio>
#include <string>
#include <SFML/Graphics.hpp>
#include "RPGParameters.h"

using namespace std;
using namespace sf;

/// @struct Coordinate
struct Coordinate {
  //! x position
  int x;

  //! y position
  int y;
};

/// @class GameObject
class GameObject {
 public:
  /// @brief default constructor
  GameObject( void );

  /// @brief destructor
  virtual ~GameObject();

  /// @brief function that sets game object name
  /// @return if the name is valid, return true. Otherwise, return false.
  bool setName( const string &name );
  
  /// @brief function that sets position of game object
  /// @param pos - game object position
  /// @return if the coordinates are not positive, return false. Otherwise, return true.
  bool setPosition( const Coordinate &pos );

  /// @brief function that gets game object name
  /// @return game object name
  string getName( void ) const {
    return name;
  }

  /// @brief function that gets object position
  /// @return game object position
  Coordinate getPosition( void ) const {
    return pos;
  }

  /// @brief function that gets game object sprite
  /// @return game object sprite reference
  const Sprite &getSprite( void ) {
    return sprite;
  }
  
  /// @brief function that sets sprite and texture of game object
  /// @note default texture is red square for all game objects
  /// @return if the setting is OK, return true. Otherwise, return false.
  virtual bool create( void );
  
  /// @brief function that prints game object information details
  /// @return -
  virtual void information( void ) = 0;

 protected:
  //! object name
  string name;

  //! object position
  Coordinate pos;
   
  //! object texture
  Texture texture;

  //! object sprite
  Sprite sprite;

};

#endif
