/**
 * @file RPGParameters.h
 * @brief RPG Parameter header
 * @date Dec 23, 2017
 * @author boo
 */

#ifndef RPG_PARAMETERS_H
#define RPG_PARAMETERS_H

//! default map size
#define MAP_SIZE ( 10 )

//! default game object name
#define GAME_OBJECT_DEFAULT_NAME ( "GameObject" )

//! invalid x position for game object
#define GAME_OBJECT_INVALID_X_COOR ( -1.0 )

//! invalid y position for game object
#define GAME_OBJECT_INVALID_Y_COOR ( -1.0 )

//! default texture path for game object
#define GAME_OBJECT_DEFAULT_TEXTURE ( ( const char * ) "asset/obstacle.png" )

//! startup gold amount for player
#define PLAYER_STARTUP_GOLD_AMOUNT ( 0 )

//! startup experience for player
#define PLAYER_STARTUP_XP ( 0 )

//! startup x position of player
#define PLAYER_STARTUP_X_COOR ( 0 )

//! startup y position of player
#define PLAYER_STARTUP_Y_COOR ( 0 )

//! maximum x coordinate for map
#define MAX_X_COOR ( MAP_SIZE )

//! maximum y coordinate for map
#define MAX_Y_COOR ( MAP_SIZE )

//! maximum line length of map file
#define MAX_MAP_FILE_LINE_LEN ( 16 )

//! maximum game object in a scene
#define MAX_GAME_OBJECT_COUNT ( 100 )

inline static int random( const int &min, const int &max ) {
  return min + ( rand() % ( max - min + 1 ) ); 
}

#endif
