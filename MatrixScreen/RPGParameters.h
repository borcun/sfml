#ifndef RPG_PARAMETERS_H
#define RPG_PARAMETERS_H

#define MAP_SIZE ( 10 )

#define GAME_OBJECT_DEFAULT_NAME ( "GameObject" )

#define GAME_OBJECT_INVALID_X_COOR ( -1 )

#define GAME_OBJECT_INVALID_Y_COOR ( -1 )

#define GAME_OBJECT_DEFAULT_TEXTURE ( ( const char * ) "asset/red_square.png" )

#define PLAYER_STARTUP_GOLD_AMOUNT ( 0 )

#define PLAYER_STARTUP_XP ( 0 )

#define PLAYER_STARTUP_X_COOR ( 0 )

#define PLAYER_STARTUP_Y_COOR ( 0 )

#define MAX_X_COOR ( MAP_SIZE )

#define MAX_Y_COOR ( MAP_SIZE )

#endif
