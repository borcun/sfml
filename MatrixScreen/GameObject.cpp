#include "GameObject.h"

GameObject::GameObject( void ) {
  name = GAME_OBJECT_DEFAULT_NAME;
  pos.x = GAME_OBJECT_INVALID_X_COOR;
  pos.y = GAME_OBJECT_INVALID_Y_COOR;
}

GameObject::~GameObject() {

}

bool GameObject::setName( const string &goName ) {
  if( 0 == goName.length() ) {
    cerr << "Game object name is empty" << endl;
    return false;
  }

  name = goName;
  
  return true;
}
 
bool GameObject::setPosition( const Coordinate &coor ) {
  if( GAME_OBJECT_INVALID_X_COOR == coor.x || GAME_OBJECT_INVALID_Y_COOR == coor.y ) {
    printf( "Game object coordinates have to be positive\n" );
    return false;
  }
  else if( MAX_X_COOR <= coor.x || MAX_Y_COOR <= coor.y ) {
    printf( "Game object coordinates have to be in (%d, %d)", MAX_X_COOR, MAX_Y_COOR );
    return false;
  }

  pos.x = coor.x;
  pos.y = coor.y;

  return true;
}

bool GameObject::create( void ) {
  if( !texture.loadFromFile( GAME_OBJECT_DEFAULT_TEXTURE ) ) {
    printf( "%s texture file is not loaded\n", GAME_OBJECT_DEFAULT_TEXTURE );
    return false;
  }

  sprite.setTexture( texture );
  return true;
}

