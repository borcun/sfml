#include "Gold.h"

Gold::Gold( const int objSign ) : GameObject( objSign ) {
  goldAmount = 0;
}

Gold::~Gold() {

}

void Gold::setGoldAmount( const int gold ) {
  if( gold < 0 ) {
    printf( "Gold amount [%d] can not be negative\n" , gold );
    return;
  }
  
  goldAmount = gold;
  return;
}

int Gold::getGoldAmount( void ) const {
  return goldAmount;
}

void Gold::information( void ) {
  printf( "%s Object\n", getName().c_str() );
  return;
}
