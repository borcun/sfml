/**
 * @file RPGScreen.h
 * @brief RPG Screen class manages game process
 * @date Dec 23, 2017
 * @author boo
 */

#ifndef RPG_SCREEN_H
#define RPG_SCREEN_H

#include "GameObjectFactory.h"
#include <unistd.h>
#include <cmath>

#define MATRIX_SIZE ( 10 )

/// @class RPGScreen
class RPGScreen {
 public:
  /// @brief constructor
  /// @param width - window width
  /// @param height - window height
  /// @param title - window title
  RPGScreen( const unsigned int width, const unsigned height, const char *title );

  /// @brief destructor
  virtual ~RPGScreen();

  /// @brief function that sets window position
  /// @param x - x position
  /// @param y - y position
  /// @return 
  void setPosition( const Vector2i &pos );

  /// @brief function that initializes RPG screen
  /// @param conf_file - configuration file path
  /// @return if the initialization is OK, return true. Otherwise, return false.
  bool init( const string &conf_file );

  /// @brief function that runs RPG screen loop
  /// @return -
  void run( void );
  
 private:
  GameObjectFactory *factory;
  RenderWindow window;
 
};

#endif
