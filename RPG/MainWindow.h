/**
 * @file MainWindow.h
 * @brief MainWindow is main window of game
 * @date Dec 23, 2017
 * @author boo
 */

#ifndef MAIN_WINDOW_H
#define MAIN_WINDOW_H

#include "GameObjectFactory.h"
#include <SFML/System.hpp>
#include <unistd.h>
#include <cmath>

//! ignore -Wswitch warnings
#pragma GCC diagnostic ignored "-Wswitch"

/// @struct ViewObject
struct RPGView {
  //! view instance
  View *view;
  
  //! view object list
  Drawable *go_list[ MAX_GAME_OBJECT_COUNT ];

  //! game object count in view
  int go_count;
};


/// @struct RPGScreen
struct RPGScreen {
  //! render window instance
  RenderWindow window;

  //! render window width
  int width;

  //! render window height
  int height;

  //! map view
  RPGView map_view;

  //! fight view
  RPGView fight_view;

  //! screen font
  Font font;
};

/// @class MainWindow
class MainWindow {
 public:
  /// @brief constructor
  /// @param width - window width
  /// @param height - window height
  /// @param title - window title
  MainWindow( const unsigned int width, const unsigned height, const char *title );

  /// @brief destructor
  virtual ~MainWindow();

  /// @brief function that sets window position
  /// @param x - x position
  /// @param y - y position
  /// @return 
  void setPosition( const Vector2i &pos );

  /// @brief function that initializes Map screen
  /// @param conf_file - configuration file path
  /// @return if the initialization is OK, return true. Otherwise, return false.
  bool init( const string &conf_file );

  /// @brief function that runs Map screen loop
  /// @return -
  void run( void );
  
 private:
  //! game factory reference
  GameObjectFactory *factory;

  //! map screen
  RPGScreen rpg_screen;

  /// @brief function that initializes map screen view
  /// @return -
  void initMapView( void );

  /// @brief function that initializes fight screen view
  /// @return -
  void initFightView( void );

  /// @brief function that updates map screen view
  /// @return -
  void updateMapView( void );

  /// @brief function that updates fight screen view
  /// @return -
  void updateFightView( void );
  
  /// @brief function that manages player wins gold
  /// @param gold - Gold reference
  /// @param x - gold x position
  /// @param y - gold y position
  /// @return -
  void winGold( Gold *gold, const int x, const int y );
 
};

#endif
