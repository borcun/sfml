/**
 * @file Gold.h
 * @brief Gold class
 * @date Dec 23, 2017
 * @author boo
 */

#ifndef GOLD_H
#define GOLD_H

#include "GameObject.h"

/// @class Gold
class Gold : public GameObject {
 public:
  /// @brief constructor
  /// @param objSign - game object sign
  Gold( const int objSign );

  /// @brief destructor
  virtual ~Gold();

  /// @brief function that sets gold amount
  /// @param gold - gold amount
  /// @return -
  void setGoldAmount( const int gold );

  /// @brief function that gets gold amount
  /// @return gold amount
  int getGoldAmount( void ) const;

  /// @brief function that prints gold object information
  /// @return -
  void information( void );
  
 private:
  //! gold amount
  int goldAmount;

};

#endif
