/**
 * @file Obstacle.h
 * @brief Obstacle class
 * @date Dec 23, 2017
 * @author boo
 */

#ifndef OBSTACLE_H
#define OBSTACLE_H

#include "GameObject.h"

/// @class Obstacle
class Obstacle : public GameObject {
 public:
  /// @brief constructor
  /// @param objSign - game object sign
  Obstacle( const int objSign );

  /// @brief destructor
  virtual ~Obstacle();

  /// @brief function that prints square object information
  /// @return -
  void information( void );

};

#endif
