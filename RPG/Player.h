/**
 * @file Player.h
 * @brief Player class
 * @date Dec 23, 2017
 * @author boo
 */

#ifndef PLAYER_H
#define PLAYER_H

#include "GameObject.h"

/// @class Player
class Player : public GameObject {
 public:
  /// @brief constructor
  /// @param obj_sign - game object sign
  Player( const int obj_sign );

  /// @brief destructor
  virtual ~Player();

  /// @brief function that sets player gold count
  /// @param pGoldCount - player gold count
  /// @return -
  void setGoldCount( const int pGoldCount );

  /// @brief function that sets player level
  /// @param pLevel - player level
  /// @return -
  void setLevel( const int pLevel );

  /// @brief function that sets player experience
  /// @param pExperience - player experience
  /// @return -
  void setExperience( const int pExperience );

  /// @brief function that sets player level up
  /// @param pToLevelUp - player level up
  /// @return -
  void setToLevelUp( const int pToLevelUp );

  /// @brief function that sets player potion count
  /// @param pPotionCount - player potion count
  /// @return -
  void setPotionCount( const int pPotionCount );

  /// @brief function that gets player gold count
  /// @return player gold count
  int getGoldCount( void ) const;

  /// @brief function that gets player level up
  /// @return player level up
  int getLevel( void ) const;

  /// @brief function that gets player experience
  /// @return player experience
  int getExperience( void ) const;

  /// @brief function that gets player level up
  /// @return player level up
  int getToLevelUp( void ) const;

  /// @brief function that gets player potion count
  /// @return player potion count
  int getPotionCount( void ) const;

  /// @brief function that is called when player wants to attack to enemy
  /// @return damage amount
  double attack( void );

  /// @brief function that prints player object information
  /// @return -
  void information( void );

  /// @brief function that is called when player wants to drink potion
  /// @return -
  void drinkPotion( void );

  /// @brief function that adds experience to player experience
  /// @param pExperience - player experience
  /// @return -
  void addExperience( const int pExperience );

  /// @brief function that adds gold to player gold amount
  /// @param goldAmount - gold amount
  /// @return -
  void addGold( const int goldAmount );

  /// @brief function that is called when player levels up
  /// @return -
  void levelUp( void );
  
 private:
  //! gold count
  int goldCount;

  //! level
  int level;

  //! experience
  int experience;

  //! level up
  int toLevelUp;

  //! potion count
  int potionCount;
};

#endif
