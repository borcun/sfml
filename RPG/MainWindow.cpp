#include "MainWindow.h"

MainWindow::MainWindow( const unsigned int width, const unsigned height, const char *title ) {
  rpg_screen.width = width;
  rpg_screen.height = height;
  
  rpg_screen.window.create( VideoMode( width, height ), title, Style::Default );
}

MainWindow::~MainWindow() {
  if( rpg_screen.window.isOpen() ) {
    rpg_screen.window.close();
  }
}

void MainWindow::setPosition( const Vector2i &pos ) {
  rpg_screen.window.setPosition( pos );
  return;
}

bool MainWindow::init( const string &conf_file ) {
  FILE *fPtr;

  factory = GameObjectFactory::instance();

  if( !rpg_screen.font.loadFromFile( "/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf" ) ) {
    printf( "Dejavu Sans font is not loaded\n" );
    return false;
  }

  if( 0 == conf_file.length() ) {
    fprintf( stderr, "Configuration file path is invalid" );
    return false;
  }

  if( NULL == ( fPtr = fopen( conf_file.c_str(), "r" ) ) ) {
    fprintf( stderr, "%s file not opened\n", conf_file.c_str() );
    return false;
  }

  while( !feof( fPtr ) ) {
    int type, pos_x, pos_y;
    char line[ MAX_MAP_FILE_LINE_LEN ] = { 0x00 };

    if( NULL != fgets( line, MAX_MAP_FILE_LINE_LEN, fPtr ) ) {
      sscanf( line, "%d %d %d", &type, &pos_x, &pos_y );
      factory->create( pos_x, pos_y, ( GameObjectType ) type );
    }
  }

  fclose( fPtr );

  for( int i=0 ; i < MAP_SIZE ; ++i ) {
    for( int j=0 ; j < MAP_SIZE ; ++j ) {
      factory->create( i, j, GOT_OBSTACLE );
    }
  }
 
  initMapView();
  initFightView();
  
  return true;
}

void MainWindow::run( void ) {
  bool is_closed = false;
  bool is_fight = false;
  
  while( !is_closed && rpg_screen.window.isOpen() ) {
    Player *player = factory->getPlayer();
    Vector2f curPos = player->getPosition();
    const int x = curPos.x / POS_SCALE_PARAM;
    const int y = curPos.y / POS_SCALE_PARAM; 
    Event event;
    
    while( rpg_screen.window.pollEvent( event ) ) {
      if( Event::Closed == event.type ) {
	is_closed = true;
      }
      else if( Event::KeyPressed == event.type ) {
	switch( event.key.code ) {
	case Keyboard::Escape: {
	  is_closed = true;
	  break;
	}
	case Keyboard::Up: {
	  GameObject *go = factory->get( x, y-1 );

	  is_fight = false;
	  
	  if( NULL != go ) {
	    Vector2f nextPos = go->getPosition();

	    switch( ( GameObjectType ) go->getSign() ) {
	    case GOT_GOLD: {
	      winGold( ( Gold * ) go, x, y-1 );
	      
	      if( factory->replace( x, y, x, y-1 ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }

	      break;
	    }
	    case GOT_FINISH_POINT: {
	      break;
	    }
	    case GOT_ENEMY: {
	      is_fight = true;
	      break;
	    }
	    case GOT_OBSTACLE: {
	      if( factory->replace( x, y, x, y-1 ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }
	    
	      break;
	    }
	    } // end of switch
	  }
	  
	  break;
	}
	case Keyboard::Down: {
	  GameObject *go = factory->get( x, y+1 );

	  is_fight = false;
	      
	  if( NULL != go ) {
	    Vector2f nextPos = go->getPosition();

	    switch( ( GameObjectType ) go->getSign() ) {
	    case GOT_GOLD: {
	      winGold( ( Gold * ) go, x, y+1 );

	      if( factory->replace( x, y, x, y+1 ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }	      
	      	      
	      break;
	    }
	    case GOT_FINISH_POINT: {
	      break;
	    }
	    case GOT_ENEMY: {
	      is_fight = true;
	      break;
	    }
	    case GOT_OBSTACLE: {
	      if( factory->replace( x, y, x, y+1 ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }
	    
	      break;
	    }
	    } // end of switch
	  }

	  break;
	}
	case Keyboard::Left: {
	  GameObject *go = factory->get( x-1, y );

	  is_fight = false;
    
	  if( NULL != go ) {
	    Vector2f nextPos = go->getPosition();
	  
	    switch( ( GameObjectType ) go->getSign() ) {
	    case GOT_GOLD: {
	      winGold( ( Gold * ) go, x-1, y );

	      if( factory->replace( x, y, x-1, y ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }

	      break;
	    }
	    case GOT_FINISH_POINT: {
	      break;
	    }
	    case GOT_ENEMY: {
	      is_fight = true;
	      break;
	    }
	    case GOT_OBSTACLE: {
	      if( factory->replace( x, y, x-1, y ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }
	    
	      break;
	    }
	    } // end of switch
	  }
	  
	  break;
	}
	case Keyboard::Right: {
	  GameObject *go = factory->get( x+1, y );

	  is_fight = false;
	  
	  if( NULL != go ) {
	    Vector2f nextPos = go->getPosition();

	    switch( ( GameObjectType ) go->getSign() ) {
	    case GOT_GOLD: {
	      winGold( ( Gold * ) go, x+1, y );
	      
	      if( factory->replace( x, y, x+1, y ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }

	      break;
	    }
	    case GOT_FINISH_POINT: {
	      break;
	    }
	    case GOT_ENEMY: {
	      is_fight = true;
	      break;
	    }
	    case GOT_OBSTACLE: {
	      if( factory->replace( x, y, x+1, y ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }
	    
	      break;
	    }
	    } // end of switch
	  }
	  
	  break;
	}
	case Keyboard::BackSpace: {
	  is_fight = false;
	  break;
	}
	} // end of switch
      }
    }

    rpg_screen.window.clear( Color::Black );
   
    if( is_fight ) {
      rpg_screen.window.setView( *rpg_screen.fight_view.view );

      updateFightView();

      for( int i=0 ; i < rpg_screen.fight_view.go_count ; ++i ) {
	if( NULL != rpg_screen.fight_view.go_list[ i ] ) {
	  rpg_screen.window.draw( *rpg_screen.fight_view.go_list[ i ] );
	}
      }
    }
    else {
      rpg_screen.window.setView( *rpg_screen.map_view.view );

      for( int i=0 ; i < MAP_SIZE ; ++i ) {
	for( int j=0 ; j < MAP_SIZE ; ++j ) {
	  if( abs( i-x ) <= 1 && abs( j-y ) <= 1 ) {
	    factory->get( i, j )->toggleTexture();
	  }
	
	  rpg_screen.window.draw( *factory->get( i, j ) );
	}
      }

      updateMapView();
      
      for( int i=0 ; i < rpg_screen.map_view.go_count ; ++i ) {
	if( NULL != rpg_screen.map_view.go_list[ i ] ) {
	  rpg_screen.window.draw( *rpg_screen.map_view.go_list[ i ] );
	}
      }
    }
    
    rpg_screen.window.display();
    
    usleep( 2000 );
  }

  return;
}

void MainWindow::initMapView( void ) {
  char xp_text[ 32 ] = { 0x00 };
  char gold_text[ 32 ] = { 0x00 };
  char potion_count_text[ 32 ] = { 0x00 };
  char level_text[ 32 ] = { 0x00 };
  int count = 0;

  // map screen view1
  rpg_screen.map_view.view = new View( FloatRect( 0, 0, rpg_screen.width, rpg_screen.height ) );
  
  sprintf( xp_text, "%10s : %d", "XP", 0 );
  sprintf( gold_text, "%10s : %d", "Gold", 0 );
  sprintf( potion_count_text, "%10s : %d", "Potion", 0 );
  sprintf( level_text, "%10s : %d", "Level", 0 );
  
  RectangleShape *info = new RectangleShape( Vector2f( 535.0f, 90.0f ) );
  info->setFillColor( Color::Green );
  info->setPosition( 5, 550 );
  rpg_screen.map_view.go_list[ count++ ] = info;
    
  GameObject *player_icon = new GameObject( GOT_OBSTACLE );
  player_icon->setName( "player_icon" );
  player_icon->setPosition( 10, 555 );
  player_icon->init( "asset/player_icon.png", "asset/player_icon.png" );
  rpg_screen.map_view.go_list[ count++ ] = player_icon;
  
  Text *xp_label = new Text( xp_text, rpg_screen.font, 16 );
  xp_label->setStyle( Text::Bold );
  xp_label->setPosition( 100, 570 );
  rpg_screen.map_view.go_list[ count++ ] = xp_label;

  Text *gold_label = new Text( gold_text, rpg_screen.font, 16 );
  gold_label->setStyle( Text::Bold );
  gold_label->setPosition( 100, 595 );
  rpg_screen.map_view.go_list[ count++ ] = gold_label;
  
  Text *pc_label = new Text( potion_count_text, rpg_screen.font, 16 );
  pc_label->setStyle( Text::Bold );
  pc_label->setPosition( 240, 570 );
  rpg_screen.map_view.go_list[ count++ ] = pc_label;
   
  Text *level_label = new Text( level_text, rpg_screen.font, 16 );
  level_label->setStyle( Text::Bold );
  level_label->setPosition( 240, 595 );  
  rpg_screen.map_view.go_list[ count++ ] = level_label;
  
  rpg_screen.map_view.go_count = count;
    
  return;
}

void MainWindow::initFightView( void ) {
  int count = 0;

  // fight screen view
  rpg_screen.fight_view.view = new View( FloatRect( rpg_screen.width, 0, rpg_screen.width, rpg_screen.height ) );

  GameObject *player_fight_icon = new GameObject( GOT_OBSTACLE );
  player_fight_icon->setName( "player_icon" );
  player_fight_icon->setPosition( 650, 150 );
  player_fight_icon->init( "asset/player_icon.png", "asset/player_icon.png" );
  rpg_screen.fight_view.go_list[ count++ ] = player_fight_icon;
  
  GameObject *enemy_fight_icon = new GameObject( GOT_OBSTACLE );
  enemy_fight_icon->setName( "enemy_icon" );
  enemy_fight_icon->setPosition( 905, 150 );
  enemy_fight_icon->init( "asset/enemy_icon.png", "asset/enemy_icon.png" );
  rpg_screen.fight_view.go_list[ count++ ] = enemy_fight_icon;
  
  GameObject *vs_icon = new GameObject( GOT_OBSTACLE );
  vs_icon->setName( "vs_icon" );
  vs_icon->setPosition( 805, 200 );
  vs_icon->init( "asset/vs.png", "asset/vs.png" );
  rpg_screen.fight_view.go_list[ count++ ] = vs_icon;

  rpg_screen.fight_view.go_count = count;
  
  return;
}

void MainWindow::updateMapView( void ) {
  char xp_text[ 32 ] = { 0x00 };
  char gold_text[ 32 ] = { 0x00 };
  char pc_text[ 32 ] = { 0x00 };
  char level_text[ 32 ] = { 0x00 };

  sprintf( xp_text, "%10s: %-8d", "XP", factory->getPlayer()->getExperience() );
  sprintf( gold_text, "%10s: %-8d", "Gold", factory->getPlayer()->getGoldCount() );
  sprintf( pc_text, "%10s: %-8d", "Potion", factory->getPlayer()->getPotionCount() );
  sprintf( level_text, "%10s: %-8d", "Level", factory->getPlayer()->getLevel() );

  ( ( Text * ) rpg_screen.map_view.go_list[ 2 ] )->setString( xp_text );
  ( ( Text * ) rpg_screen.map_view.go_list[ 3 ] )->setString( gold_text );
  ( ( Text * ) rpg_screen.map_view.go_list[ 4 ] )->setString( pc_text );
  ( ( Text * ) rpg_screen.map_view.go_list[ 5 ] )->setString( level_text );
  
  return;
}

void MainWindow::updateFightView( void ) {
  return;
}

void MainWindow::winGold( Gold *gold, const int x, const int y ) {
  factory->getPlayer()->addGold( gold->getGoldAmount() );
	      
  printf( "Gold Amount : %d\n", gold->getGoldAmount() );
  printf( "Player Gold Amount : %d\n", factory->getPlayer()->getGoldCount() );
	      
  if( factory->destroy( x, y ) ) {
    if( factory->create( x, y, GOT_OBSTACLE ) ) {
      printf( "Gold object is changed to obstacle object (%d, %d)\n", x, y );
    }
  }
  
  return;
}
