#include "RPGScreen.h"

RPGScreen::RPGScreen( const unsigned int width, const unsigned height, const char *title  ) {
  window.create( VideoMode( width, height ), title, Style::Default );
}

RPGScreen::~RPGScreen() {
  if( window.isOpen() ) {
    window.close();
  }
}

void RPGScreen::setPosition( const Vector2i &pos ) {
  window.setPosition( pos );
  return;
}

bool RPGScreen::init( const string &conf_file ) {
  if( 0 == conf_file.length() ) {
    printf( "Configuration file path is invalid" );
    return false;
  }

  // read configuration file
  // parse game object

  factory = GameObjectFactory::instance();

  Vector2< int > player_pos = { 0, 0 };
  Vector2< int > enemy1_pos = { 1, 1 };
  Vector2< int > enemy2_pos = { 2, 2 };
  Vector2< int > enemy3_pos = { 3, 3 };
  Vector2< int > gold1_pos  = { 3, 8 };
  Vector2< int > gold2_pos  = { 4, 8 };
  Vector2< int > gold3_pos  = { 8, 4 };
  Vector2< int > fp_pos = { 0, 8 };

  for( int i=0 ; i < MAP_SIZE ; ++i ) {
    for( int j=0 ; j < MAP_SIZE ; ++j ) {
      if( i == player_pos.x && j == player_pos.y ) {
	factory->create( i, j, GOT_PLAYER );
      }
      else if( i == enemy1_pos.x && j == enemy1_pos.y ) {
	factory->create( i, j, GOT_ENEMY );
      }
      else if( i == enemy2_pos.x && j == enemy2_pos.y ) {
	factory->create( i, j, GOT_ENEMY );
      }
      else if( i == enemy3_pos.x && j == enemy3_pos.y ) {
	factory->create( i, j, GOT_ENEMY );
      }
      else if( i == gold1_pos.x && j == gold1_pos.y ) {
	factory->create( i, j, GOT_GOLD );
      }
      else if( i == gold2_pos.x && j == gold2_pos.y ) {
	factory->create( i, j, GOT_GOLD );
      }
      else if( i == gold3_pos.x && j == gold3_pos.y ) {
	factory->create( i, j, GOT_GOLD );
      }
      else if( i == fp_pos.x && j == fp_pos.y ) {
	factory->create( i, j, GOT_FINISH_POINT );
      }
      else {	
	factory->create( i, j, GOT_SQUARE );
      }
    }
  }

  return true;
}

void RPGScreen::run( void ) {
  bool isClosed = false;
  // int goldCnt = 0;
  // int enemyCnt = 0;
  
  RectangleShape info( Vector2f( 545.0f, 90.0f ) );
  RectangleShape topAlign( Vector2f( 555.0f, 10.0f ) );
  RectangleShape leftAlign( Vector2f( 10.0f, 660.0f ) );
  
  info.setFillColor( Color::Green );
  info.setPosition( 10, 565 );
  
  topAlign.setFillColor( Color::Black );
  topAlign.setPosition( 0, 0 );
    
  leftAlign.setFillColor( Color::Black );
  leftAlign.setPosition( 0, 0 );

  while( !isClosed && window.isOpen() ) {
    Event event;

    while( window.pollEvent( event ) ) {
      if( Event::Closed == event.type ) {
	isClosed = true;
      }
      else if( Event::KeyPressed == event.type ) {
	switch( event.key.code ) {
	case Keyboard::Escape: {
	  isClosed = true;
	  break;
	}
	case Keyboard::Up: {
	  Player *player = factory->getPlayer();
	  Vector2f curPos = player->getPosition();

	  int x = curPos.x / POS_SCALE_PARAM;
	  int y = curPos.y / POS_SCALE_PARAM;
	  
	  GameObject *go = factory->get( x, y - 1 );
	  Vector2f nextPos = go->getPosition();

	  switch( ( GameObjectType ) go->getSign() ) {
	  case GOT_GOLD: {
	    break;
	  }
	  case GOT_FINISH_POINT: {
	    break;
	  }
	  case GOT_ENEMY: {
	    break;
	  }
	  case GOT_SQUARE: {
	    // if array indices are changed, change graphical position
	    if( factory->replace( x, y, x, y - 1 ) ) {
	      player->setPosition( nextPos );
	      go->setPosition( curPos );
	    }
	    
	    break;
	  }
	  } // end of switch
	  
	  break;
	}
	case Keyboard::Down: {
	  Player *player = factory->getPlayer();
	  Vector2f curPos = player->getPosition();

	  int x = curPos.x / POS_SCALE_PARAM;
	  int y = curPos.y / POS_SCALE_PARAM;
	  
	  GameObject *go = factory->get( x, y + 1 );
	  Vector2f nextPos = go->getPosition();

	  switch( ( GameObjectType ) go->getSign() ) {
	  case GOT_GOLD: {
	    break;
	  }
	  case GOT_FINISH_POINT: {
	    break;
	  }
	  case GOT_ENEMY: {
	    break;
	  }
	  case GOT_SQUARE: {
	    // if array indices are changed, change graphical position
	    if( factory->replace( x, y, x, y + 1 ) ) {
	      player->setPosition( nextPos );
	      go->setPosition( curPos );
	    }
	    
	    break;
	  }
	  } // end of switch
	  
	  break;
	}
	case Keyboard::Left: {
	  Player *player = factory->getPlayer();
	  Vector2f curPos = player->getPosition();

	  int x = curPos.x / POS_SCALE_PARAM;
	  int y = curPos.y / POS_SCALE_PARAM;
	  
	  GameObject *go = factory->get( x - 1, y );
	  Vector2f nextPos = go->getPosition();

	  
	  switch( ( GameObjectType ) go->getSign() ) {
	  case GOT_GOLD: {
	    break;
	  }
	  case GOT_FINISH_POINT: {
	    break;
	  }
	  case GOT_ENEMY: {
	    break;
	  }
	  case GOT_SQUARE: {
	    // if array indices are changed, change graphical position
	    if( factory->replace( x, y, x - 1, y ) ) {
	      player->setPosition( nextPos );
	      go->setPosition( curPos );
	    }
	    
	    break;
	  }
	  } // end of switch

	  break;
	}
	case Keyboard::Right: {
	  Player *player = factory->getPlayer();
	  Vector2f curPos = player->getPosition();

	  int x = curPos.x / POS_SCALE_PARAM;
	  int y = curPos.y / POS_SCALE_PARAM;
	  
	  GameObject *go = factory->get( x + 1, y );
	  Vector2f nextPos = go->getPosition();

	  
	  switch( ( GameObjectType ) go->getSign() ) {
	  case GOT_GOLD: {
	    break;
	  }
	  case GOT_FINISH_POINT: {
	    break;
	  }
	  case GOT_ENEMY: {
	    break;
	  }
	  case GOT_SQUARE: {
	    // if array indices are changed, change graphical position
	    if( factory->replace( x, y, x + 1, y ) ) {
	      player->setPosition( nextPos );
	      go->setPosition( curPos );
	    }
	    
	    break;
	  }
	  } // end of switch

	  break;
	}
	} // end of switch
      }
    }

    window.clear( Color::Black );

    for( int i=0 ; i < MAP_SIZE ; ++i ) {
      for( int j=0 ; j < MAP_SIZE ; ++j ) {
	window.draw( *factory->get( i, j ) );
      }
    }
    
    window.draw( info );
    
    window.display();
    usleep( 2000 );
  }

  return;
}
  
