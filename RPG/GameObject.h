/**
 * @file GameObject.h
 * @brief Game Object class
 * @date Dec 23, 2017
 * @author boo
 */

#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include <iostream>
#include <cstdio>
#include <string>
#include <SFML/Graphics.hpp>
#include "RPGParameters.h"

using namespace std;
using namespace sf;

/// @class GameObject
class GameObject : public Sprite {
 public:
  /// @brief constructor
  /// @param obj_sign - game object sign
  GameObject( const int obj_sign );

  /// @brief destructor
  virtual ~GameObject();

  /// @brief function that gets game object sign
  /// @return game object sign
  int getSign( void ) const;
  
  /// @brief function that sets game object name
  /// @return if the name is valid, return true. Otherwise, return false.
  bool setName( const string &name );

  /// @brief function that gets game object name
  /// @return game object name
  string getName( void ) const;
 
  /// @brief function that initializes game object, sets sprite and texture of game object
  /// @param real_tp - real texture path
  /// @param fake_tp - fake texture path
  /// @return if the setting is OK, return true. Otherwise, return false.
  bool init( const string &real_tp, const string &fake_tp );

  /// @brief function that toggles texture of sprite
  /// @return -
  void toggleTexture( void );
  
  /// @brief function that prints game object information details
  /// @return -
  virtual void information( void );

 protected:
  //! game object sign
  int sign;
  
 private:
  //! game object name
  string name;

  //! texture file path
  string tex_path;

  //! is texture toggled flag
  bool is_texture_toggled;
  
  //! real game object texture
  Texture real_texture;

  //! fake game object texture
  Texture fake_texture;
};

#endif
