#include "MapScreen.h"

MapScreen::MapScreen( const unsigned int width, const unsigned height, const char *title  ) {
  window.create( VideoMode( width, height ), title, Style::Default );
}

MapScreen::~MapScreen() {
  if( window.isOpen() ) {
    window.close();
  }
}

void MapScreen::setPosition( const Vector2i &pos ) {
  window.setPosition( pos );
  return;
}

bool MapScreen::init( const string &conf_file ) {
  FILE *fPtr;
 
  if( 0 == conf_file.length() ) {
    fprintf( stderr, "Configuration file path is invalid" );
    return false;
  }

  // read configuration file
  if( NULL == ( fPtr = fopen( conf_file.c_str(), "r" ) ) ) {
    fprintf( stderr, "%s file not opened\n", conf_file.c_str() );
    return false;
  }
  
  // parse game object
  factory = GameObjectFactory::instance();

  while( !feof( fPtr ) ) {
    int type, pos_x, pos_y;
    char line[ 16 ] = { 0x00 };

    if( NULL != fgets( line, 16, fPtr ) ) {
      sscanf( line, "%d %d %d", &type, &pos_x, &pos_y );
      factory->create( pos_x, pos_y, ( GameObjectType ) type );
    }
  }

  fclose( fPtr );

  for( int i=0 ; i < MAP_SIZE ; ++i ) {
    for( int j=0 ; j < MAP_SIZE ; ++j ) {
      factory->create( i, j, GOT_OBSTACLE );
    }
  }
  
  return true;
}

void MapScreen::run( void ) {
  RectangleShape info( Vector2f( 535.0f, 90.0f ) );
  GameObject player_icon( GOT_OBSTACLE );
  GameObject fight_icon( GOT_OBSTACLE );
  GameObject player_fight_icon( GOT_OBSTACLE );
  GameObject enemy_fight_icon( GOT_OBSTACLE );
  GameObject vs_icon( GOT_OBSTACLE );
  bool is_closed = false;
  bool is_fight = false;
  Color black = Color::Black;
  View map_view( FloatRect( 0, 0, 545, 642 ) );
  View fight_view( FloatRect( 545, 0, 545, 642 ) );
  Font font;
  
  // Load it from a file
  if( !font.loadFromFile( "/usr/share/fonts/truetype/dejavu/DejaVuSansMono.ttf" ) ) {
    printf( "Dejavu Sans font is not loaded\n" );
    return;
  }
  
  info.setFillColor( Color::Green );
  info.setPosition( 5, 550 );
 
  player_fight_icon.setName( "player_icon" );
  player_fight_icon.setPosition( 650, 150 );
  player_fight_icon.init( "asset/player_icon.png", "asset/player_icon.png" );
 
  enemy_fight_icon.setName( "enemy_icon" );
  enemy_fight_icon.setPosition( 905, 150 );
  enemy_fight_icon.init( "asset/enemy_icon.png", "asset/enemy_icon.png" );

  vs_icon.setName( "vs_icon" );
  vs_icon.setPosition( 805, 200 );
  vs_icon.init( "asset/vs.png", "asset/vs.png" );

  player_icon.setName( "player_icon" );
  player_icon.setPosition( 10, 555 );
  player_icon.init( "asset/player_icon.png", "asset/player_icon.png" );

  fight_icon.setName( "fight_icon" );
  fight_icon.setPosition( 450, 555 );
  fight_icon.init( "asset/fight_icon.png", "asset/fight_icon.png" );

  char xp_text[ 32 ] = { 0x00 };
  char gold_text[ 32 ] = { 0x00 };
  char potion_count_text[ 32 ] = { 0x00 };
  char level_text[ 32 ] = { 0x00 };

  sprintf( xp_text, "%10s : %d", "XP", 0 );
  sprintf( gold_text, "%10s : %d", "Gold", 0 );
  sprintf( potion_count_text, "%10s : %d", "Potion", 0 );
  sprintf( level_text, "%10s : %d", "Level", 0 );
  
  Text xp_label( xp_text, font, 16 );
  Text gold_label( gold_text, font, 16 );
  Text potion_count_label( gold_text, font, 16 );
  Text level_label( gold_text, font, 16 );

  xp_label.setColor( black );
  xp_label.setStyle( Text::Bold );
  xp_label.setPosition( 100, 570 );
  
  gold_label.setColor( black );
  gold_label.setStyle( Text::Bold );
  gold_label.setPosition( 100, 595 );
  
  potion_count_label.setColor( black );
  potion_count_label.setStyle( Text::Bold );
  potion_count_label.setPosition( 240, 570 );
  
  level_label.setColor( black );
  level_label.setStyle( Text::Bold );
  level_label.setPosition( 240, 595 );
  
  while( !is_closed && window.isOpen() ) {
    Player *player = factory->getPlayer();
    Vector2f curPos = player->getPosition();
    const int x = curPos.x / POS_SCALE_PARAM;
    const int y = curPos.y / POS_SCALE_PARAM; 
    char xp_text[ 32 ] = { 0x00 };
    char gold_text[ 32 ] = { 0x00 };
    Event event;
    
    while( window.pollEvent( event ) ) {
      if( Event::Closed == event.type ) {
	is_closed = true;
      }
      else if( Event::KeyPressed == event.type ) {
	switch( event.key.code ) {
	case Keyboard::Escape: {
	  is_closed = true;
	  break;
	}
	case Keyboard::Up: {
	  GameObject *go = factory->get( x, y-1 );

	  is_fight = false;
	  
	  if( NULL != go ) {
	    Vector2f nextPos = go->getPosition();

	    switch( ( GameObjectType ) go->getSign() ) {
	    case GOT_GOLD: {
	      winGold( ( Gold * ) go, x, y-1 );
	      
	      if( factory->replace( x, y, x, y-1 ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }

	      break;
	    }
	    case GOT_FINISH_POINT: {
	      break;
	    }
	    case GOT_ENEMY: {
	      is_fight = true;
	      break;
	    }
	    case GOT_OBSTACLE: {
	      if( factory->replace( x, y, x, y-1 ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }
	    
	      break;
	    }
	    } // end of switch
	  }
	  
	  break;
	}
	case Keyboard::Down: {
	  GameObject *go = factory->get( x, y+1 );

	  is_fight = false;
	      
	  if( NULL != go ) {
	    Vector2f nextPos = go->getPosition();

	    switch( ( GameObjectType ) go->getSign() ) {
	    case GOT_GOLD: {
	      winGold( ( Gold * ) go, x, y+1 );

	      if( factory->replace( x, y, x, y+1 ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }	      
	      	      
	      break;
	    }
	    case GOT_FINISH_POINT: {
	      break;
	    }
	    case GOT_ENEMY: {
	      is_fight = true;
	      break;
	    }
	    case GOT_OBSTACLE: {
	      if( factory->replace( x, y, x, y+1 ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }
	    
	      break;
	    }
	    } // end of switch
	  }

	  break;
	}
	case Keyboard::Left: {
	  GameObject *go = factory->get( x-1, y );

	  is_fight = false;
    
	  if( NULL != go ) {
	    Vector2f nextPos = go->getPosition();
	  
	    switch( ( GameObjectType ) go->getSign() ) {
	    case GOT_GOLD: {
	      winGold( ( Gold * ) go, x-1, y );

	      if( factory->replace( x, y, x-1, y ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }

	      break;
	    }
	    case GOT_FINISH_POINT: {
	      break;
	    }
	    case GOT_ENEMY: {
	      is_fight = true;
	      break;
	    }
	    case GOT_OBSTACLE: {
	      if( factory->replace( x, y, x-1, y ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }
	    
	      break;
	    }
	    } // end of switch
	  }
	  
	  break;
	}
	case Keyboard::Right: {
	  GameObject *go = factory->get( x+1, y );

	  is_fight = false;
	  
	  if( NULL != go ) {
	    Vector2f nextPos = go->getPosition();

	    switch( ( GameObjectType ) go->getSign() ) {
	    case GOT_GOLD: {
	      winGold( ( Gold * ) go, x+1, y );
	      
	      if( factory->replace( x, y, x+1, y ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }

	      break;
	    }
	    case GOT_FINISH_POINT: {
	      break;
	    }
	    case GOT_ENEMY: {
	      is_fight = true;
	      break;
	    }
	    case GOT_OBSTACLE: {
	      if( factory->replace( x, y, x+1, y ) ) {
		player->setPosition( nextPos );
		go->setPosition( curPos );
	      }
	    
	      break;
	    }
	    } // end of switch
	  }
	  
	  break;
	}
	case Keyboard::BackSpace: {
	  is_fight = false;
	  break;
	}
	} // end of switch
      }
    }

    window.clear( Color::Black );
   
    for( int i=0 ; i < MAP_SIZE ; ++i ) {
      for( int j=0 ; j < MAP_SIZE ; ++j ) {
	if( abs( i-x ) <= 1 && abs( j-y ) <= 1 ) {
	  factory->get( i, j )->toggleTexture();
	}
	
	window.draw( *factory->get( i, j ) );
      }
    }
    
    window.draw( info );
    window.draw( player_icon );

    sprintf( xp_text, "%10s: %-8d", "XP", factory->getPlayer()->getExperience() );
    sprintf( gold_text, "%10s: %-8d", "Gold", factory->getPlayer()->getGoldCount() );
    sprintf( potion_count_text, "%10s: %-8d", "Potion", factory->getPlayer()->getPotionCount() );
    sprintf( level_text, "%10s: %-8d", "Level", factory->getPlayer()->getLevel() );

    xp_label.setString( xp_text );
    gold_label.setString( gold_text );
    potion_count_label.setString( potion_count_text );
    level_label.setString( level_text );
      
    window.draw( xp_label );
    window.draw( gold_label );
    window.draw( potion_count_label );
    window.draw( level_label );
    window.draw( player_fight_icon );
    window.draw( enemy_fight_icon );
    window.draw( vs_icon );
    
    if( is_fight ) {
      window.setView( fight_view );
    }
    else {
      window.setView( map_view );
    }
    
    window.display();
    usleep( 2000 );
  }

  return;
}

void MapScreen::winGold( Gold *gold, const int x, const int y ) {
  factory->getPlayer()->addGold( gold->getGoldAmount() );
	      
  printf( "Gold Amount : %d\n", gold->getGoldAmount() );
  printf( "Player Gold Amount : %d\n", factory->getPlayer()->getGoldCount() );
	      
  if( factory->destroy( x, y ) ) {
    if( factory->create( x, y, GOT_OBSTACLE ) ) {
      printf( "Gold object is changed to obstacle object (%d, %d)\n", x, y );
    }
  }
  
  return;
}
