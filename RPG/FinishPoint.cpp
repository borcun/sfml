#include "FinishPoint.h"

FinishPoint::FinishPoint( const int objSign ) : GameObject( objSign ) {

}

FinishPoint::~FinishPoint() {

}

void FinishPoint::information( void ) {
  printf( "%s Object\n", getName().c_str() );
  return;
}
