/**
 * @file Enemy.h
 * @brief Enemy class
 * @date Dec 23, 2017
 * @author boo
 */

#ifndef ENEMY_H
#define ENEMY_H

#include "GameObject.h"

/// @class Enemy
class Enemy : public GameObject {
 public:
  /// @brief constructor
  /// @param objSign - game object sign
  Enemy( const int objSign );

  /// @brief destructor
  virtual ~Enemy();

  /// @brief function that sets enemy experience
  /// @param eExperience - enemy experience
  /// @return -
  void setExperience( const int eExperience );

  /// @brief function that sets enemy potion chance
  /// @param ePotionChance - enemy potion chance
  /// @return -
  void setPotionChance( const double ePotionChance );

  /// @brief function that gets enemy experience
  /// @return enemy experience
  int getExperience( void ) const;

  /// @brief function that gets enemy potion chance
  /// @return enemy potion chance
  double getPotionChance( void ) const;

  /// @brief function that prints enemy information
  /// @return -
  void information( void );

  /// @brief function that is called when enemy wants to drink potion
  /// @return -
  void drinkPotion( void );
  
 private:
  //! experience
  int experience;

  //! potion chance
  double potionChance;

};

#endif
