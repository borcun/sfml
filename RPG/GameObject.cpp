#include "GameObject.h"

GameObject::GameObject( const int obj_sign ) {
  name = GAME_OBJECT_DEFAULT_NAME;
  tex_path = GAME_OBJECT_DEFAULT_TEXTURE;
  sign = obj_sign;
  is_texture_toggled = false;
  
  setPosition( GAME_OBJECT_INVALID_X_COOR, GAME_OBJECT_INVALID_Y_COOR );
}

GameObject::~GameObject() {

}

int GameObject::getSign( void ) const {
  return sign;
}

bool GameObject::setName( const string &go_name ) {
  if( 0 == go_name.length() ) {
    cerr << "Game object name is empty" << endl;
    return false;
  }

  name = go_name;
  
  return true;
}

string GameObject::getName( void ) const {
  return name;
}

bool GameObject::init( const string &real_tp, const string &fake_tp ) {
  if( 0 == real_tp.length() ) {
    cerr << "Real game object texture file path is empty" << endl;
    return false;
  }

  if( 0 == fake_tp.length() ) {
    cerr << "Fake game object texture file path is empty" << endl;
    return false;
  }

  if( !real_texture.loadFromFile( real_tp ) ) {
    printf( "%s real texture file is not loaded\n", real_tp.c_str() );
    return false;
  }

  
  if( !fake_texture.loadFromFile( fake_tp ) ) {
    printf( "%s fake texture file is not loaded\n", fake_tp.c_str() );
    return false;
  }

  setTexture( fake_texture );
  return true;
}

void GameObject::toggleTexture( void ) {
  if( !is_texture_toggled ) {
    setTexture( real_texture );
    is_texture_toggled = true;
  }

  return;
}

void GameObject::information( void ) {
  printf( "%s Object\n", getName().c_str() );
  return;
};
