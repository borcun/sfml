#include "RPGScreen.h"

RPGScreen::RPGScreen( const unsigned int width, const unsigned height, const char *title  ) {
  window.create( VideoMode( width, height ), title, Style::Default );
}

RPGScreen::~RPGScreen() {
  if( window.isOpen() ) {
    window.close();
  }
}

void RPGScreen::setPosition( const unsigned int x, const unsigned int y ) {
  window.setPosition( Vector2i( x, y ) );
  return;
}

void RPGScreen::run( void ) {
  bool isClosed = false;
  int goldCnt = 0;
  int enemyCnt = 0;
  
  RectangleShape info( Vector2f( 590.0f, 90.0f ) );

  info.setFillColor( Color::Green );
  info.setPosition( 50, 630 );

  if( !player.tex.loadFromFile( "asset/player.png" ) ) {
    cerr << "The player texture is not loaded" << endl;
    return;
  }

  if( !gold[0].tex.loadFromFile( "asset/gold.png" ) ) {
    cerr << "The gold texture is not loaded" << endl;
    return;
  }

  if( !gold[1].tex.loadFromFile( "asset/gold.png" ) ) {
    cerr << "The gold texture is not loaded" << endl;
    return;
  }

  if( !finishPoint.tex.loadFromFile( "asset/fp.png" ) ) {
    cerr << "The finish point texture is not loaded" << endl;
    return;
  }
    
  if( !enemy[0].tex.loadFromFile( "asset/enemy.png" ) ) {
    cerr << "The enemy texture is not loaded" << endl;
    return;
  }
    
  if( !enemy[1].tex.loadFromFile( "asset/enemy.png" ) ) {
    cerr << "The enemy texture is not loaded" << endl;
    return;
  }
    
  if( !enemy[2].tex.loadFromFile( "asset/enemy.png" ) ) {
    cerr << "The enemy texture is not loaded" << endl;
    return;
  }
    
  player.spr.setTexture( player.tex );
  player.spr.setScale( 0.12f, 0.1f );
  
  gold[0].spr.setTexture( gold[0].tex );
  gold[0].spr.setScale( 0.4f, 0.4f );
  
  gold[1].spr.setTexture( gold[1].tex );
  gold[1].spr.setScale( 0.4f, 0.4f );

  finishPoint.spr.setTexture( finishPoint.tex );
  finishPoint.spr.setScale( 0.4f, 0.4f );

  enemy[0].spr.setTexture( enemy[0].tex );
  enemy[0].spr.setScale( 0.2f, 0.2f );

  enemy[1].spr.setTexture( enemy[1].tex );
  enemy[1].spr.setScale( 0.2f, 0.2f );

  enemy[2].spr.setTexture( enemy[2].tex );
  enemy[2].spr.setScale( 0.2f, 0.2f );

  for( int i=0 ; i < MATRIX_SIZE ; ++i ) {
    vector< RectangleShape > vec;
    
    for( int j=0 ; j < MATRIX_SIZE ; ++j ) {
      if( MATRIX_SIZE - 1 == i && 0 == j ) {
	player.spr.setPosition( ( j * 60 + 50 ), ( i * 60 + 20 ) );
      }
      else if( ( 3 == i && 3 == j ) || ( 4 == i && 6 == j ) ) {
	gold[ goldCnt++ ].spr.setPosition( ( j * 60 + 50 ), ( i * 60 + 20 ) );
      }
      else if( 0 == i && 8 == j ) {
	finishPoint.spr.setPosition( ( j * 60 + 50 ), ( i * 60 + 20 ) );
      }
      else if( ( 7 == i && 7 == j ) ||
	       ( 4 == i && 5 == j ) ||
	       ( 3 == i && 1 == j ) )
      {
	enemy[ enemyCnt++ ].spr.setPosition( ( j * 60 + 50 ), ( i * 60 + 20 ) );
      }
      else {
	RectangleShape rect( Vector2f( 50.0f, 50.0f ) );
    
	rect.setFillColor( Color::Red );
	rect.setPosition( ( j * 60 + 50 ), ( i * 60 + 20 ) );
	
	vec.push_back( rect );
      }
    }

    rectList.push_back( vec );
  }
    
  while( !isClosed && window.isOpen() ) {
    Event event;

    while( window.pollEvent( event ) ) {
      if( Event::Closed == event.type ||
	  ( Event::KeyPressed == event.type && Keyboard::Escape == event.key.code ) )
      {
	isClosed = true;
      }
    }

    window.clear( Color::Black );

    window.draw( player.spr );

    window.draw( gold[0].spr );
    window.draw( gold[1].spr );
    
    window.draw( finishPoint.spr );

    window.draw( enemy[0].spr );
    window.draw( enemy[1].spr );
    window.draw( enemy[2].spr );

    for( int i=0 ; i < rectList.size() ; ++i ) {
      for( int j=0 ; j < rectList[i].size() ; ++j ) {
	window.draw( rectList[i][j] );
      }
    }

    window.draw( info );
    
    window.display();
    usleep( 2000 );
  }

  return;
}
  
