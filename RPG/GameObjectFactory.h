/**
 * @file GameObjectFactory.h
 * @brief Game Object Factory class
 * @date Dec 23, 2017
 * @author boo
 */

#ifndef GAME_OBJECT_FACTORY_H
#define GAME_OBJECT_FACTORY_H

#include "Gold.h"
#include "FinishPoint.h"
#include "Enemy.h"
#include "Player.h"
#include "Obstacle.h"
#include <vector>

//! position scale parameter
#define POS_SCALE_PARAM ( 55 )

//! set position macro
#define SET_POSITION( x ) ( ( x ) * ( POS_SCALE_PARAM ) )


/// @enum GameObjectType
enum GameObjectType {
  //! gold game object type
  GOT_GOLD,

  //! finish point game object type
  GOT_FINISH_POINT,

  //! enemy game object type
  GOT_ENEMY,

  //! player game object type
  GOT_PLAYER,

  //! obstacle game object type
  GOT_OBSTACLE,

  //! other game object type
  GOT_OTHER
};


/// @class GameObjectFactory
class GameObjectFactory {
 public:
  /// @brief function that gets class instance
  /// @return class instance
  static GameObjectFactory *instance( void );
  
  /// @brief destructor
  virtual ~GameObjectFactory();

  /// @brief function that creates game object
  /// @note if there is already game object, it is deleted, then new one is created
  /// @param x - game object x position
  /// @param y - game object y position
  /// @param got - game object type
  /// @return if game object is created, return true. Otherwise, return false.
  bool create( const int x, const int y, GameObjectType got );

  /// @brief function that destroys game object
  /// @param x - game object x position
  /// @param y - game object y position
  /// @return if game object is destroyed, return true. Otherwise, return false.
  bool destroy( const int x, const int y );

  /// @brief function that gets game object which is indicated by coordinate
  /// @param x - game object x position
  /// @param y - game object y position
  /// @return game object reference, or NULL if pos is empty
  GameObject *get( const int x, const int y );

  /// @brief function that replace two coordinate
  /// @param x1 - first x coordinate
  /// @param y1 - first y coordinate
  /// @param x2 - second x coordinate
  /// @param y2 - second y coordinate
  /// @return if the replacement is OK, return true. Otherwise, return false
  bool replace( const int x1, const int y1, const int x2, const int y2 );
  
  /// @brief function that gets Player object
  /// @return Player reference
  Player *getPlayer( void );
  
 private:
  //! game object reference
  static GameObjectFactory *goFactory;

  //! game object list
  GameObject *gameObjects[ MAP_SIZE ][ MAP_SIZE ];

  //! player game object
  Player *mPlayer;

  //! flag which indicates that player object is created
  bool isPlayerCreated;

  //! flag which indicates that finish point object is created
  bool isFinishPointCreated;

  /// @brief default constructor
  GameObjectFactory( void );

};


#endif
