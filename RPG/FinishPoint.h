/**
 * @file FinishPoint.h
 * @brief Finish Point Game Object class
 * @date Dec 23, 2017
 * @author boo
 */

#ifndef FINISH_POINT_H
#define FINISH_POINT_H

#include "GameObject.h"

/// @class FinishPoint
class FinishPoint : public GameObject {
 public:
  /// @brief constructor
  /// @param objSign - game object sign
  FinishPoint( const int objSign );

  /// @brief destructor
  virtual ~FinishPoint();

  /// @brief function that prints finish point information
  /// @return -
  void information( void );

};

#endif
