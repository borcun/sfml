#include "Enemy.h"

Enemy::Enemy( const int objSign ) : GameObject( objSign ) {

}

Enemy::~Enemy() {

}

void Enemy::setExperience( const int eExperience ) {
  if( eExperience < 0 ) {
    printf( "Enemy experience amount can not be negative\n" );
    return;
  }

  experience = eExperience;
  return;
}

void Enemy::setPotionChance( const double ePotionChance ) {
  if( ePotionChance < 0 ) {
    printf( "Enemy potion chance can not be negative\n" );
    return;
  }

  potionChance = ePotionChance;
  return;
}

int Enemy::getExperience( void ) const {
  return experience;
}

double Enemy::getPotionChance( void ) const {
  return potionChance;
}

void Enemy::information( void ) {
  printf( "%s Object\n", getName().c_str() );
  return;
}

void Enemy::drinkPotion( void ) {
  printf( "Enemy drunk potion\n" );
  return;
}

