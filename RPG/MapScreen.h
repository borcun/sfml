/**
 * @file MapScreen.h
 * @brief Map Screen class manages game process
 * @date Dec 23, 2017
 * @author boo
 */

#ifndef MAP_SCREEN_H
#define MAP_SCREEN_H

#include "GameObjectFactory.h"
#include <SFML/System.hpp>
#include <unistd.h>
#include <cmath>

//! ignore -Wswitch warnings
#pragma GCC diagnostic ignored "-Wswitch"

/// @class MapScreen
class MapScreen {
 public:
  /// @brief constructor
  /// @param width - window width
  /// @param height - window height
  /// @param title - window title
  MapScreen( const unsigned int width, const unsigned height, const char *title );

  /// @brief destructor
  virtual ~MapScreen();

  /// @brief function that sets window position
  /// @param x - x position
  /// @param y - y position
  /// @return 
  void setPosition( const Vector2i &pos );

  /// @brief function that initializes Map screen
  /// @param conf_file - configuration file path
  /// @return if the initialization is OK, return true. Otherwise, return false.
  bool init( const string &conf_file );

  /// @brief function that runs Map screen loop
  /// @return -
  void run( void );
  
 private:
  //! game factory reference
  GameObjectFactory *factory;

  //! render window
  RenderWindow window;

  //! fight window
  RenderWindow fight_window;

  /// @brief function that manages player wins gold
  /// @return -
  void winGold( Gold *gold, const int x, const int y );
 
};

#endif
