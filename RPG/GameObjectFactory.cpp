#include "GameObjectFactory.h"

GameObjectFactory *GameObjectFactory::goFactory = NULL;

GameObjectFactory *GameObjectFactory::instance( void ) {
  if( NULL == goFactory ) {
    goFactory = new GameObjectFactory();
  }

  return goFactory;
}

GameObjectFactory::GameObjectFactory( void ) {
  srand( time( NULL ) );
  
  isPlayerCreated = false;
  isFinishPointCreated = false;
  
  for( int i=0 ; i < MAP_SIZE ; ++i ) {
    for( int j=0 ; j < MAP_SIZE ; ++j ) {
      gameObjects[ i ][ j ] = NULL;
    }
  }
}

GameObjectFactory::~GameObjectFactory() {
  for( int i=0 ; i < MAP_SIZE ; ++i ) {
    for( int j=0 ; j < MAP_SIZE ; ++j ) {
      if( NULL != gameObjects[ i ][ j ] ) {
	delete gameObjects[ i ][ j ];
      }
    }
  }
}

bool GameObjectFactory::create( const int x, const int y, GameObjectType got ) {
  bool isCreated = false;
  
  switch( got ) {
  case GOT_GOLD: {
    if( NULL == gameObjects[ x ][ y ] ) {
      Gold *gold = new Gold( GOT_GOLD );

      if( NULL != gold ) {
	isCreated = gold->init( "asset/gold.png", "asset/obstacle.png" );

	if( !isCreated ) {
	  printf( "Gold object can not created\n" );

	  gameObjects[ x ][ y ] = NULL;
	  delete gold;
	}
	else {
	  gold->setPosition( SET_POSITION( x ), SET_POSITION( y ) );
	  gold->setName( "Gold" );
	  gold->setGoldAmount( random( 100, 300 ) );

	  gameObjects[ x ][ y ] = gold;
	}
      }
      else {
	printf( "Gold object can not created\n" );
      }
    }
    else {
      // printf( "There is already Game Object in position (%d,%d)\n", x, y );
      return false;
    }
    
    break;
  }
  case GOT_FINISH_POINT: {
    if( isFinishPointCreated ) {
      printf( "Finish Point object was already created\n" );
    }
    else {
      if( NULL == gameObjects[ x ][ y ] ) {
	FinishPoint *finishPoint = new FinishPoint( GOT_FINISH_POINT );

	if( NULL != finishPoint ) {
	  isCreated = finishPoint->init( "asset/fp.png", "asset/obstacle.png" );

	  if( !isCreated ) {
	    printf( "Finish Point object can not created\n" );

	    gameObjects[ x ][ y ] = NULL;
	    delete finishPoint;
	  }
	  else {
	    finishPoint->setPosition( SET_POSITION( x ), SET_POSITION( y ) );
	    finishPoint->setName( "Finish Point" );

	    gameObjects[ x ][ y ] = finishPoint;
	  
	    isFinishPointCreated = true;
	  }
	}
	else {
	  printf( "Finish Point object can not created\n" );
	}
      }
      else {
	// printf( "There is already Game Object in position (%d,%d)\n", x, y );
	return false;
      }
    }
    
    break;
  }
  case GOT_ENEMY: {
    if( NULL == gameObjects[ x ][ y ] ) {
      Enemy *enemy = new Enemy( GOT_ENEMY );

      if( NULL != enemy ) {
	isCreated = enemy->init( "asset/enemy.png", "asset/obstacle.png" );

	if( !isCreated ) {
	  printf( "Enemy object can not created\n" );
	  delete enemy;
	}
	else {
	  enemy->setPosition( SET_POSITION( x ), SET_POSITION( y ) );
	  enemy->setName( "Enemy" );
	  enemy->setExperience( random( 500, 1000 ) );
	  enemy->setPotionChance( 3 );

	  gameObjects[ x ][ y ] = enemy;
	}
      }
      else {
	printf( "Enemy object can not created\n" );
      }
    }
    else {
      // printf( "There is already Game Object in position (%d,%d)\n", x, y );
      return false;
    }
    
    break;
  }
  case GOT_PLAYER: {
    if( isPlayerCreated ) {
      printf( "Player object was already created\n" );
    }
    else {
      if( NULL == gameObjects[ x ][ y ] ) {
	Player *player = new Player( GOT_PLAYER );

	if( NULL != player ) {
	  isCreated = player->init( "asset/player.png", "asset/player.png" );

	  if( !isCreated ) {
	    printf( "Player object can not created\n" );

	    gameObjects[ x ][ y ] = NULL;
	    delete player;
	  }
	  else {
	    player->setPosition( SET_POSITION( x ), SET_POSITION( y ) );
	    player->setName( "Player" );
	    player->setExperience( 500 );
	    player->setGoldCount( 0 );
	    player->setLevel( 0 );
	    player->setPotionCount( 5 );

	    gameObjects[ x ][ y ] = player;
	    mPlayer = player;
	  
	    isPlayerCreated = true;
	  }
	}
	else {
	  printf( "Player object can not created\n" );
	}
      }
      else {
	// printf( "There is already Game Object in position (%d,%d)\n", x, y );
	return false;
      }
    }
    
    break;
  }
  case GOT_OBSTACLE: {
    if( NULL == gameObjects[ x ][ y ] ) {
      Obstacle *obstacle = new Obstacle( GOT_OBSTACLE );

      if( NULL != obstacle ) {
	isCreated = obstacle->init( "asset/obstacle.png", "asset/obstacle.png" );

	if( !isCreated ) {
	  printf( "Obstacle object can not created\n" );
	  delete obstacle;
	}
	else {
	  obstacle->setPosition( SET_POSITION( x ), SET_POSITION( y ) );
	  obstacle->setName( "Obstacle" );

	  gameObjects[ x ][ y ] = obstacle;
	}
      }
      else {
	printf( "Obstacle object can not created\n" );
      }
    }
    else {
      // printf( "There is already Game Object in position (%d,%d)\n", x, y );
      return false;
    }
    
    break;
  }
  default: {
    printf( "Invalid Game Object Type [%d]", ( int ) got );
    break;
  }
  } // end of switch

  return isCreated;
}


bool GameObjectFactory::destroy( const int x, const int y ) {
  if( x < MAP_SIZE && y < MAP_SIZE && x >= 0 && y >= 0 ) {
    if( NULL != gameObjects[ x ][ y ] ) {
      delete gameObjects[ x ][ y ];
      gameObjects[ x ][ y ] = NULL;
      
      return true;
    }
  }

  return false;
}

GameObject *GameObjectFactory::get( const int x, const int y ) {
  if( x < MAP_SIZE && y < MAP_SIZE && x >= 0 && y >= 0 ) {
    if( NULL != gameObjects[ x ][ y ] ) {
      return gameObjects[ x ][ y ];
    }
  }

  return NULL;
}

bool GameObjectFactory::replace( const int x1, const int y1, const int x2, const int y2 ) {
  GameObject *firstObject = get( x1, y1 );
  GameObject *secondObject = get( x2, y2 );

  if( NULL != firstObject && NULL != secondObject ) {
    gameObjects[ x1 ][ y1 ] = secondObject;
    gameObjects[ x2 ][ y2 ] = firstObject;

    return true;
  }

  return false;
}

Player *GameObjectFactory::getPlayer( void ) {
  return mPlayer;
}
