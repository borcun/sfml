#ifndef MATRIX_SCREEN_H
#define MATRIX_SCREEN_H

#include <iostream>
#include <SFML/Graphics.hpp>
#include <unistd.h>
#include <vector>

#define MATRIX_SIZE ( 10 )

using namespace std;
using namespace sf;

typedef vector< vector< RectangleShape > > RectList;

struct GameObject {
  Texture tex;
  Sprite spr;
};

// Matrix Screen class
class MatrixScreen {
 public:
  MatrixScreen( const unsigned int width, const unsigned height, const char *title  );
  virtual ~MatrixScreen();
  void setPosition( const unsigned int x, const unsigned int y );
  void run( void );
  
 private:
  RenderWindow window;
  RectList rectList;
  GameObject player;
  GameObject gold[ 2 ];
  GameObject finishPoint;
  GameObject enemy[ 3 ];
};

#endif
