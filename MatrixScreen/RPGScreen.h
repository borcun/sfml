/**
 * @file RPGScreen.h
 * @brief RPG Screen class manages game process
 * @date Dec 23, 2017
 * @author boo
 */

#ifndef RPG_SCREEN_H
#define RPG_SCREEN_H

#include <iostream>
#include <SFML/Graphics.hpp>
#include <unistd.h>
#include <vector>

#define MATRIX_SIZE ( 10 )

using namespace std;
using namespace sf;

/// @class RPGScreen
class RPGScreen {
 public:
  RPGScreen( const unsigned int width, const unsigned height, const char *title );
  virtual ~RPGScreen();
  void setPosition( const unsigned int x, const unsigned int y );
  void run( void );
  
 private:
  RenderWindow window;
  vector< GameObject > rectList;
  GameObject player;
  GameObject gold[ 2 ];
  GameObject finishPoint;
  GameObject enemy[ 3 ];
};

#endif
